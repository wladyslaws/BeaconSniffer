//
//  ViewController.swift
//  BeaconSniffer
//
//  Created by WLADYSLAW SURALA on 16/03/2017.
//  Copyright © 2017 wladyslaws. All rights reserved.
//

import Cocoa
import CoreLocation
import CoreBluetooth

class ViewController: NSViewController, CBCentralManagerDelegate {

    var cbManager : CBCentralManager!
    var queue : DispatchQueue!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.queue = DispatchQueue(label: "beaconQueue")
        self.cbManager = CBCentralManager(delegate: self, queue: self.queue)
//        let uuid = UUID(uuidString: "your uuid here")
//        let cbUUID = CBUUID(nsuuid: uuid!)
        self.cbManager.scanForPeripherals(withServices: nil, options: nil)
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            print("manager is on")
        default:
            print("manager is not on")
        }

    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print(advertisementData)
        print(peripheral)
    }

}

